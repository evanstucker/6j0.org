Bootstrapping a company

# Install Arch Linux
TODO: Why Arch? 
https://wiki.archlinux.org/title/Installation_guide

# Install yay
TODO: Why yay?
https://github.com/Jguer/yay

# Install radicle
TODO: Why radicle?
```
yay -S radicle-cli
```

# Create the bootstrap git repo for 6j0.org
```
mkdir -p ~/radicle.xyz/6j0.org
cd ~/radicle.xyz/6j0.org
git init
rad init
rad push
```

# Create a Hetzner account
TODO: Why Hetzner?
https://www.hetzner.com/cloud

# Install Terraform
https://developer.hashicorp.com/terraform/downloads?product_intent=terraform

# Create a server
```
export TF_VAR_hcloud_token=REDACTED
terraform init
terraform apply
```

# Log into the server
You need to log into Hetzner to find the server's IP, because I'm too lazy to make the terraform output it right now. 
TODO: Make terraform output the IP, lazyass.
```
ssh root@REDACTED_IP_ADDRESS
```

# Install Kubernetes?
Use kops on Hetzner? https://kops.sigs.k8s.io/getting_started/hetzner/
Or maybe use kubespray? https://kubespray.io/#/

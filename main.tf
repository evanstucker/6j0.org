terraform {
  required_providers {
    hcloud = {
      source = "hetznercloud/hcloud"
      version = "1.36.0"
    }
  }
}

variable "hcloud_token" {
  sensitive = true # Requires terraform >= 0.14
}

provider "hcloud" {
  token = var.hcloud_token
}

resource "hcloud_ssh_key" "tiberiu" {
  name       = "tiberiu"
  public_key = file("~/.ssh/id_ed25519.pub")
}

# https://registry.terraform.io/providers/hetznercloud/hcloud/latest/docs/resources/server
# TODO: Is Ubuntu the best choice? Which one has the most free memory after installing?
resource "hcloud_server" "node1" {
  name        = "node1"
  server_type = "cpx21"
  image       = "ubuntu-22.04"
  location    = "ash"
  ssh_keys    = [
    "tiberiu"
  ]
}

# https://registry.terraform.io/providers/hetznercloud/hcloud/latest/docs/resources/volume
# TODO: Can volumes be encrypted?
resource "hcloud_volume" "master" {
  name      = "volume1"
  size      = 10
  server_id = hcloud_server.node1.id
  automount = true
  format    = "ext4"
}
